from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required, user_passes_test

from berita.models import Kategori, Artikel
from berita.forms import ArtikelForm

# Create your views here.
def is_operator(user):
    if user.groups.filter(name = 'Operator').exists():
        return True
    else:
        return False


@login_required
def dashboard(request):
    template_name = "dashboard/index.html"
    context = {
        'title': 'halaman dashboard'        
    }
    return render(request, template_name, context)

@login_required
@user_passes_test(is_operator, login_url='/authentikasi/logout')
def kategori_list(request):
    template_name = "dashboard/snippets/kategori_list.html"
    kategori = Kategori.objects.all() 
    context = {
        'title': 'halaman kategori',
        'kategori': kategori
    }
    return render(request, template_name, context)

@login_required
@user_passes_test(is_operator, login_url='/authentikasi/logout')
def kategori_add(request):
    template_name = "dashboard/snippets/kategori_add.html" 
    if request.method == "POST":
        nama_input = request.POST.get('nama_kategori')
        Kategori.objects.create(
            nama = nama_input
        )
        return redirect('kategori_list')
    
    context = {
        'title': 'tambah kategori',
    }
    return render(request, template_name, context)

@login_required
@user_passes_test(is_operator, login_url='/authentikasi/logout')
def kategori_update(request, id_kategori):
    template_name = "dashboard/snippets/kategori_update.html"
    kategori = get_object_or_404(Kategori, id=id_kategori)
    
    if request.method == "POST":
        nama_input = request.POST.get('nama_kategori')
        kategori.nama = nama_input
        kategori.save()
        return redirect('kategori_list')
    
    context = {
        'title': 'update kategori',
        'kategori': kategori
    }
    return render(request, template_name, context)

@login_required
@user_passes_test(is_operator, login_url='/authentikasi/logout')
def kategori_delete(request, id_kategori):
    try:
        Kategori.objects.get(id=id_kategori).delete()
    except Kategori.DoesNotExist:
        pass
    return redirect('kategori_list')

@login_required
def artikel_list(request):
    template_name = "dashboard/snippets/artikel_list.html"
    if request.user.groups.filter(name='Operator '):
        artikel = Artikel.objects.all()
    else:
        artikel = Artikel.objects.filter(author=request.user)
    context = {
        'title': 'daftar artikel',
        'artikel': artikel
    }
    return render(request, template_name, context)

@login_required
@user_passes_test(is_operator, login_url='/authentikasi/logout')
def artikel_add(request):
    template_name = "dashboard/snippets/artikel_forms.html"
    
    if request.method == "POST":
        form = ArtikelForm(request.POST, request.FILES)
        if form.is_valid():
            artikel = form.save(commit=False)
            artikel.author = request.user
            artikel.save()
            return redirect(artikel_list)
    else:
        form = ArtikelForm()
    
    context = {
        'title': 'Tambah Artikel',
        'form': form,
    }
    return render(request, template_name, context)

@login_required
@user_passes_test(is_operator, login_url='/authentikasi/logout')
def artikel_detail(request, id_artikel):
    template_name = "dashboard/snippets/artikel_detail.html"
    artikel = get_object_or_404(Artikel, id=id_artikel)
    context = {
        'title': artikel.judul,
        'artikel': artikel
    }
    return render(request, template_name, context)

@login_required
@user_passes_test(is_operator, login_url='/authentikasi/logout')
def artikel_update(request, id_artikel):
    template_name = "dashboard/snippets/artikel_forms.html"
    artikel = get_object_or_404(Artikel, id=id_artikel)
    
    if request.user.groups.filter(name='Operator'):
        pass
    else:
        if artikel.author != request.user:
            return redirect('/')
    
    if request.method == "POST":
        form = ArtikelForm(request.POST, request.FILES, instance=artikel)
        if form.is_valid():
            artikel = form.save(commit=False)
            artikel.author = request.user
            artikel.save()
            return redirect('artikel_list')
    
    form = ArtikelForm(instance=artikel)
    context = {
        'title': artikel.judul,
        'form': form
    }
    return render(request, template_name, context)

@login_required

def artikel_delete(request, id_artikel):
    try:
        artikel = Artikel.objects.get(id=id_artikel)
        if request.user.groups.filter(name='Operator'):
            pass
        else:
            if artikel.author != request.user:
                return redirect('/')
            artikel.delete()
    except:pass
    return redirect('artikel_list')
