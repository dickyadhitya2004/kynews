from django.db import models

#Create your models here.

class Prodi(models.Model):

    kode_prodi = models. CharField(max_length=255)
    nama_prodi = models. CharField(max_length=255)
    tahun_berdiri = models. CharField(max_length=255)
    fakultas = models.CharField(max_length=255)
    
    def __str__(self):
        return f"{self.kode_prodi} - {self.nama_prodi}"

class Mahasiswa (models.Model):
    username = models. CharField(max_length=255)
    nama = models.CharField(max_length=255)
    umur = models.IntegerField()
    prodi = models.ManyToManyField(Prodi)
    
    def __str__(self):
        return f"(self.username) (self.nama)"
    
class SosialMedia (models.Model):
    username = models.OneToOneField(Mahasiswa, on_delete=models.CASCADE)
    facebook = models. CharField(max_length=255)

    def __str__(self):
        return f"(self.username) - {self.facebook}"



#kasih catatan penjelasarinya dari 3 tabel yang dibuat
'''
jelaskan disini
'''