from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

def akun_login(request):
    if request.user.is_authenticated:
        return redirect('/')
    
    template_name = "halaman/login.html"
    pesan = ''
    
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            pesan = "Gagal login. Silakan cek kembali username dan password Anda."

    context = {
        'pesan': pesan
    }
    return render(request, template_name, context)


def akun_registrasi(request):
    if request.user.is_authenticated:
        return redirect('/')
    
    pesan = ''
    template_name = "halaman/registrasi.html"
    
    if request.method == "POST":
        username = request.POST.get("username")
        nama_depan = request.POST.get("nama_depan")
        nama_belakang = request.POST.get("nama_belakang")
        email = request.POST.get("email")
        password1 = request.POST.get("password1")
        password2 = request.POST.get("password2")

        if password1 == password2:
            # Periksa apakah username sudah digunakan
            if User.objects.filter(username=username).exists():
                pesan = "Username sudah digunakan. Silakan pilih username lain."
            else:
                # Buat objek User baru
                new_user = User.objects.create_user(
                    username=username,
                    email=email,
                    password=password1,
                    first_name=nama_depan,
                    last_name=nama_belakang,
                    is_active=True
                )
                login(request, new_user)  # Otomatis login setelah registrasi
                return redirect('/')
        else:
            pesan = "Password tidak sama. Silakan masukkan password yang sama pada kedua kolom."

    context = {
        'pesan': pesan
    }
    return render(request, template_name, context)


def akun_logout(request):
    logout(request)
    return redirect('/')